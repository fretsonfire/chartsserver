VERSION=1.0
NAME=FretsOnFire-ChartsServer-$(VERSION)

dist:
	@mkdir -p dist/$(NAME)
	@cp -r *.py charts log templates media README COPYING INSTALL CHANGELOG Makefile dist/$(NAME)
	@cd dist && tar --exclude .svn --exclude "*~" -czf $(NAME).tar.gz $(NAME) && cd ..
	@rm -rf dist/$(NAME)

.PHONY: dist
