import sys
import os
import time
from fretsweb.settings import LOG_FILE

quiet    = False
logFile  = None

encoding = "iso-8859-1"

if "-v" in sys.argv:
  quiet = False
  
if os.name == "posix":
  labels = {
    "warn":   "\033[1;33m(W)\033[0m",
    "debug":  "\033[1;34m(D)\033[0m",
    "notice": "\033[1;32m(N)\033[0m",
    "error":  "\033[1;31m(E)\033[0m",
  }
else:
  labels = {
    "warn":   "(W)",
    "debug":  "(D)",
    "notice": "(N)",
    "error":  "(E)",
  }

def log(cls, msg):
  msg = unicode(msg).encode(encoding, "ignore")
  if not quiet:
    print labels[cls] + " " + time.asctime() + ": " + msg
  if LOG_FILE:
    logFile = open(LOG_FILE, "a+")
    print >>logFile, labels[cls] + " " + time.asctime() + ": " + msg
    logFile.close()

warn   = lambda msg: log("warn", msg)
debug  = lambda msg: log("debug", msg)
notice = lambda msg: log("notice", msg)
error  = lambda msg: log("error", msg)
