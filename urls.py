from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^fretsonfire/charts/',             include('fretsweb.charts.urls')),
    (r'^fretsonfire/admin/',              include('django.contrib.admin.urls')),
    (r'^fretsonfire/media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/skyostil/proj/fretsweb/media'}),
    (r'fretsonfire/play/?$',              'fretsweb.charts.views.play'),
)
