#################################################################################
# Frets On Fire Charts Server configuration
#################################################################################

# Full path to this Django project
FRETSONFIRE_HOME = "" 

# URL that handles the media served from the media directory.
MEDIA_URL = '/fretsonfire/media/'

# How many scores to show on the main page
MAX_SCORES = 20

# After how many seconds a ban will be automatically lifted
BAN_LIFT_SECONDS = 60 * 60 * 12

# Charts cache refresh threshold in seconds and games
# The cache will be refreshed when CACHE_REFRESH_SECONDS
# seconds pass and every CACHE_REFRESH_SCORES scores.
CACHE_REFRESH_SECONDS = 60 * 15
CACHE_REFRESH_SCORES  = 1

#################################################################################
# End of configurable settings
#################################################################################

# Absolute path to the directory that holds media.
MEDIA_ROOT = '%s/media' % FRETSONFIRE_HOME

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/fretsonfire/media/admin/'

# Server log file
LOG_FILE = "%s/log/server.log" % FRETSONFIRE_HOME

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '%s/templates' % FRETSONFIRE_HOME
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.humanize',
    'fretsweb.charts',
)
