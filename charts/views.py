import Cerealizer
import binascii
import sha
import datetime
import time
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.decorators.cache import cache_page
from django.db import connection
from django.contrib.auth.decorators import login_required

from fretsweb.charts.models import Song, Score, Difficulty, Player, BannedAddress, refreshCache
from fretsweb.charts.song import decodeScores, ScoreException, HashException, getArtistAndTitleForSong
from fretsweb.charts.utils import search, paginate
from fretsweb import log
from fretsweb.settings import MAX_SCORES, BAN_LIFT_SECONDS, DEBUG, \
                              CACHE_REFRESH_SECONDS, CACHE_REFRESH_SCORES

def charts(request):
  cursor = connection.cursor()

  # Best total scores
  cursor.execute("""SELECT player_id, SUM(best_score) AS s FROM best_scores
                    GROUP BY player_id
                    ORDER BY s DESC LIMIT %s""", [MAX_SCORES])
  topScores  = [{"name": name, "score": score} for name, score in cursor.fetchall()]

  # Most played songs
  cursor.execute("""SELECT song_id, COUNT(song_id) FROM charts_score
                    GROUP BY song_id
                    ORDER BY count(song_id) DESC LIMIT %s""", [MAX_SCORES])
  topSongs   = [Song.objects.get(pk = name) for name, count in cursor.fetchall()]
  
  # Most games played
  cursor.execute("""SELECT charts_player.name, count(charts_score.score) as c FROM charts_player, charts_score
                    WHERE charts_player.name=charts_score.player_id
                    GROUP BY charts_player.name
                    ORDER BY c DESC LIMIT %s""", [MAX_SCORES])
  topPlayers = [{"name": name, "count": count} for name, count in cursor.fetchall()]

  # Most earned medals
  cursor.execute("""SELECT s1.player_id, s1.count, s2.count, s3.count
                    FROM (player_gold_medals AS s1 LEFT JOIN player_silver_medals AS s2 ON s1.player_id=s2.player_id)
                    LEFT JOIN player_bronze_medals AS s3 ON s3.player_id=s2.player_id
                    ORDER BY s1.count DESC, s2.count DESC, s3.count DESC LIMIT %s""", [MAX_SCORES])

  def integer(n):
    if n is None:
      return 0
    return int(n)

  topMedals = [{"name": name, "gold": integer(c1), "silver": integer(c2), "bronze": integer(c3)} for name, c1, c2, c3 in cursor.fetchall()]

  # Recent scores
  cursor.execute("""SELECT player_id, charts_song.name, charts_song.title, score FROM charts_score, charts_song
                    WHERE charts_score.song_id = charts_song.name AND charts_song.verified=1
                    GROUP BY player_id, song_id
                    ORDER BY date desc LIMIT 5""")
  recentScores = [{"playerName": playerName, "songName": songName, "songTitle": songTitle, "score": score} \
                  for playerName, songName, songTitle, score in cursor.fetchall()]

  namespace = {
    "topSongs":           topSongs,
    "topPlayers":         topPlayers,
    "topScores":          topScores,
    "topMedals":          topMedals,
    "recentScores":       recentScores,
    "playCount":          Score.objects.all().count(),
    "playerCount":        Player.objects.all().count(),
    "songCount":          Song.objects.all().count(),
    "verifiedSongCount":  Song.objects.filter(verified = True).count(),
  }
  
  return render_to_response('charts/charts.html', namespace)
charts = cache_page(charts, 60 * 15)

def songList(request):
  namespace, objects = search(request, Song, "name__icontains")
  objects = objects.filter(verified = True)
  namespace.update(paginate(request, objects))
  return render_to_response('charts/songs.html', namespace)

def song(request, songId):
  song      = get_object_or_404(Song, pk = songId)

  if not song.verified:
    raise Http404

  def scoresForDifficulty(difficulty):
    scores       = song.score_set.all().filter(difficulty = difficulty).order_by("-score")[:MAX_SCORES * 2]
    uniqueScores = []
    players      = set()
    for score in scores:
      if not score.player_id in players:
        players.add(score.player_id)
        uniqueScores.append(score)
    return uniqueScores[:MAX_SCORES]

  scoreList = []
  for difficulty in Difficulty.objects.all():
    scoreList += scoresForDifficulty(difficulty)

  namespace = {
    "song": song,
    "scoreList": scoreList,
  }
  
  return render_to_response('charts/song.html', namespace)

def playerList(request):
  namespace, objects = search(request, Player, "name__icontains")
  objects = objects.filter(banned = False)
  namespace.update(paginate(request, objects))
  return render_to_response('charts/players.html', namespace)

def player(request, playerId):
  player = get_object_or_404(Player, pk = playerId)

  if player.banned:
    raise Http404

  namespace = {
    "player": player,
  }
  
  return render_to_response('charts/player.html', namespace)

def play(request):
  songId    = request["songName"]
  songHash  = request["songHash"]
  version   = request["version"]
  scores    = request["scores"]
  address   = request.META.get("REMOTE_ADDR", None)
  timestamp = request.GET.get("timestamp", None)

  # Log the event
  log.notice("New scores from game version %s at %s for song '%s'." % (version, address, songId))

  # Lift expired bans
  liftThreshold = datetime.datetime.now() - datetime.timedelta(0, BAN_LIFT_SECONDS)
  BannedAddress.objects.filter(date__lt = liftThreshold).delete()

  def banSubmitter(reason):
    if address:
      log.error("%s. Banning address '%s'." % (reason, address))
      BannedAddress.objects.create(address = address, date = datetime.datetime.now(), reason = reason)

  def response(reply, reason = None):
    if reply != "True":
      log.error("%s: %s" % (reply, reason))
    if DEBUG and reason:
      return HttpResponse("%s: %s" % (reply, reason))
    return HttpResponse(reply)
  
  # Check whether the address is banned
  try:
    ban = BannedAddress.objects.get(pk = address)
    log.error("Address '%s' is has been banned at %s. Rejecting scores." % (address, ban.date))
    if not DEBUG:
      return response("False", "Address banned")
  except BannedAddress.DoesNotExist:
    pass

  # Decode the scores
  try:
    scores = decodeScores(scores)
  except ScoreException, e:
    banSubmitter("Unable to decode scores: %s." % (e))
    return response("False", e)
  except HashException, e:
    # A hash mismatch is not grounds for a ban -- just don't record the score
    return response("False", e)

  # Check the song name
  badChars = "\\/?&"
  if badChars in songId or not songId:
    return response("False", "Bad song name")

  # If the song doesn't exist, try to find one with a matching hash. If that fails, create a new song
  try:
    song = Song.objects.get(pk = songId)
  except Song.DoesNotExist:
    try:
      song = Song.objects.filter(hash = songHash)[0]
      log.notice("Song '%s' is actually '%s'." % (songId, song.name))
    except IndexError:
      artist, title = getArtistAndTitleForSong(songId)
      song = Song.objects.create(name = songId, title = title, artist = artist, hash = songHash)

  # Write all scores to the database
  for difficulty, score, stars, name in scores:
    # Check that the player or song name doesn't contain funny characters
    if badChars in name or not name:
      return response("False", "Bad player name")

    if score == 0:
      continue

    if len(name) > 16:
      name = name[:16]

    # If the player doesn't exist, create a new player
    try:
      player = Player.objects.get(pk = name)
    except Player.DoesNotExist:
      player = Player.objects.create(name = name)

    # If the player is banned, ban this IP too
    if player.banned:
      banSubmitter("Player '%s' is banned. Therefore the address '%s' will also be banned." % (player, address))
      return response("False", "Player is banned")

    # If this exact same score already exists, ignore it
    try:
      Score.objects.get(song = song, player = player, difficulty = difficulty, \
                        score = score, stars = stars)
    except Score.DoesNotExist:
      date = datetime.datetime.now()
      Score.objects.create(song = song, player = player, difficulty = difficulty, \
                           date = date, score = score, stars = stars)

      # See if we should refresh the charts cache
      refreshThreshold = datetime.datetime.now() - datetime.timedelta(0, CACHE_REFRESH_SECONDS)
      if Score.objects.all().count() % CACHE_REFRESH_SCORES == 0 or \
         Score.objects.filter(date__gt = refreshThreshold).count() == 1:
        log.notice("Refreshing charts cache. %d scores recorded." % Score.objects.all().count())
        refreshCache()

  return HttpResponse("True")
play = cache_page(play, 0)

@login_required
def refresh(request):
  refreshCache()
  return HttpResponseRedirect('/fretsonfire/admin')
refresh = cache_page(refresh, 0)

@login_required
def topUnverifiedSongs(request):
  songs = Song.objects.filter(verified = False)
  songs = sorted(songs, key = lambda song: -song.score_set.count())
  songs = songs[:MAX_SCORES]
  return render_to_response('charts/topsongs.html', {"songs": songs})

@login_required
def mergeDuplicateSongs(request):
  dupSongCount  = 0
  dupScoreCount = 0
  result        = ""

  result += '<ul>'
  for song in Song.objects.filter(verified = True):
    result += '<li>%s<ul>' % song.name
    for dupSong in Song.objects.filter(hash = song.hash, verified = False):
      dupSongCount += 1

      result += '<li>%s</li>' % dupSong.name
      for score in dupSong.score_set.all():
        score.song = song
        score.save()
        dupScoreCount += 1

      if dupSong.score_set.count() == 0:
        dupSong.delete()
    result += '</ul></li>'
  result += '</ul>'

  result += "<p>%d duplicate songs with %d scores found and merged.</p>" % (dupSongCount, dupScoreCount)
  return render_to_response('charts/result.html', {"operation": "Merge duplicate songs", "result": result})

def documentation(request):
  return render_to_response('charts/documentation.html', {})
