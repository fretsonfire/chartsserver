from django.db import models
from django.db import connection

class Song(models.Model):
  name     = models.CharField(maxlength = 64, primary_key = True)
  title    = models.CharField(maxlength = 128)
  artist   = models.CharField(maxlength = 128, blank = True)
  verified = models.BooleanField(default = False)
  label    = models.ImageField(upload_to = "labels/%Y-%m-%d-%H%M", default = "", blank = True)
  hash     = models.CharField(maxlength = 128, db_index = True)

  def __str__(self):
    if self.artist:
      return "%s by %s" % (self.title, self.artist)
    return "%s" % (self.title)

  def playCount(self):
    return self.score_set.count()

  def playCountOrdinal(self):
    # Get all scores for this song
    scores  = self.song.score_set.all().order_by("-score")
    players = set()

    # Remove duplicate players until this score is found
    for score in scores:
      if not score.player_id in players:
        players.add(score.player_id)
      if score == self:
        return len(players)

  class Admin:
    search_fields = ['name', 'title', 'artist']
    list_display  = ['__str__', 'verified', 'playCount']
    list_filter   = ['verified']

class Player(models.Model):
  name       = models.CharField(maxlength = 64, primary_key = True)
  password   = models.CharField(maxlength = 64, default = "", blank = True)
  banned     = models.BooleanField(default = False)

  class Admin:
    search_fields = ['name']
    list_display  = ['__str__', 'playCount', 'totalScore', 'banned']

  class Meta:
    ordering = ['name']

  def playCount(self):
    return self.score_set.count()
  
  def __str__(self):
    return self.name

  def totalScore(self):
    cursor = connection.cursor()
    cursor.execute("""SELECT SUM(best_score) FROM best_scores
                      WHERE player_id=%s""", [self.name])
    try:
      return int(cursor.fetchone()[0])
    except:
      return 0

  def medals(self, ordinal):
    cursor = connection.cursor()
    if ordinal > 1:
      cursor.execute("""SELECT *, COUNT(s2.player_id) as ordinal FROM best_scores as s1, best_scores as s2
                        WHERE s1.player_id=%s AND s2.best_score>s1.best_score AND s1.song_id=s2.song_id AND
                              s1.difficulty_id=s2.difficulty_id
                        GROUP BY s1.song_id, s1.difficulty_id
                        HAVING ordinal=%s""", [self.name, ordinal - 1])
    elif ordinal == 1:
      cursor.execute("""SELECT * FROM best_scores AS s1, best_song_scores AS s2
                        WHERE s1.player_id=%s AND s1.best_score=s2.best_score AND s1.song_id=s2.song_id AND
                              s1.difficulty_id=s2.difficulty_id
                        GROUP BY s1.song_id, s1.difficulty_id""", [self.name])
    else:
      raise RuntimeError("Bad ordinal: %d" % ordinal)
    return len(cursor.fetchall())

  def goldMedals(self):
    return self.medals(1)

  def silverMedals(self):
    return self.medals(2)

  def bronzeMedals(self):
    return self.medals(3)

class Difficulty(models.Model):
  name       = models.CharField(maxlength = 64)
  identifier = models.IntegerField(db_index = True)

  def __str__(self):
    return self.name

  class Meta:
    ordering            = ['identifier']
    verbose_name_plural = "difficulties"

  class Admin:
    pass

class Score(models.Model):
  MAX_STARS = 5

  song       = models.ForeignKey(Song)
  player     = models.ForeignKey(Player)
  difficulty = models.ForeignKey(Difficulty)
  date       = models.DateTimeField()
  score      = models.IntegerField(db_index = True)
  stars      = models.IntegerField(db_index = True)

  class Admin:
    search_fields  = ['song__name', 'player__name']
    date_hierarchy = 'date'

  class Meta:
    ordering = ['-score']

  def __str__(self):
    return '%s: %d points on %s difficulty by %s' % (self.song.name, self.score, self.difficulty.name, self.player.name)

  def starList(self):
    return [True] * self.stars + [False] * (Score.MAX_STARS - self.stars)

  def ordinal(self):
    cursor = connection.cursor()
    cursor.execute("""SELECT 1 FROM charts_score
                      WHERE song_id=%s AND difficulty_id=%s AND score>%s
                      GROUP BY player_id
                      ORDER BY score DESC""", [self.song.name, self.difficulty.id, self.score])
    return len(cursor.fetchall()) + 1
    """
    # Get all scores for this song and difficulty
    scores  = self.song.score_set.filter(difficulty = self.difficulty).order_by("-score")
    players = set()

    # Remove duplicate players until this score is found
    for score in scores:
      if not score.player_id in players:
        players.add(score.player_id)
      if score == self:
        return len(players)
    """

class BannedAddress(models.Model):
  address = models.CharField(maxlength = 128, primary_key = True)
  date    = models.DateTimeField()
  reason  = models.CharField(maxlength = 256, default = "", blank = True)

  class Admin:
    search_fields  = ['address']
    date_hierarchy = 'date'

  class Meta:
    ordering            = ['date']
    verbose_name_plural = "banned addresses"

  def __str__(self):
    return "%s: %s" % (self.address, self.reason)

class BestScores(models.Model):
  player     = models.ForeignKey(Player)
  best_score = models.IntegerField(db_index = True)
  song       = models.ForeignKey(Song)
  difficulty = models.ForeignKey(Difficulty)

  class Meta:
    db_table = "best_scores"

class BestSongScores(models.Model):
  song       = models.ForeignKey(Song)
  difficulty = models.ForeignKey(Difficulty)
  best_score = models.IntegerField(db_index = True)

  class Meta:
    db_table = "best_song_scores"

class SecondBestSongScores(models.Model):
  song       = models.ForeignKey(Song)
  difficulty = models.ForeignKey(Difficulty)
  best_score = models.IntegerField(db_index = True)

  class Meta:
    db_table = "second_best_song_scores"

class ThirdSongScores(models.Model):
  song       = models.ForeignKey(Song)
  difficulty = models.ForeignKey(Difficulty)
  best_score = models.IntegerField(db_index = True)

  class Meta:
    db_table = "third_best_song_scores"

class PlayerGoldMedals(models.Model):
  player     = models.ForeignKey(Player)
  count      = models.IntegerField(db_index = True)

  class Meta:
    db_table = "player_gold_medals"


class PlayerSilverMedals(models.Model):
  player     = models.ForeignKey(Player)
  count      = models.IntegerField(db_index = True)

  class Meta:
    db_table = "player_silver_medals"


class PlayerBronzeMedals(models.Model):
  player     = models.ForeignKey(Player)
  count      = models.IntegerField(db_index = True)

  class Meta:
    db_table = "player_bronze_medals"

def refreshCache():
  try:
    # Do the whole refresh process in a transaction
    cursor = connection.cursor()
    cursor.execute("""START TRANSACTION""")

    # Create a view that lists all the best scores for a player
    cursor.execute("""DELETE FROM best_scores""")
    cursor.execute("""INSERT INTO best_scores (player_id, best_score, song_id, difficulty_id)
                      SELECT player_id, max(score) AS best_score, song_id, difficulty_id FROM charts_score, charts_song
                      WHERE charts_score.song_id=charts_song.name AND charts_song.verified=1
                      GROUP BY song_id, difficulty_id, player_id order by score desc""")
    cursor.fetchall()

    # Create a table for the best song scores by difficulty
    cursor.execute("""DELETE FROM best_song_scores""")
    cursor.execute("""INSERT INTO best_song_scores (song_id, difficulty_id, best_score)
                      SELECT b.song_id, b.difficulty_id, MAX(b.best_score) AS best_score FROM best_scores as b, charts_song
                      WHERE b.song_id=charts_song.name AND charts_song.verified=1
                      GROUP BY b.song_id, b.difficulty_id
                      ORDER BY b.best_score DESC""")
    
    # Create a table for the second best song scores by difficulty
    cursor.execute("""DELETE FROM second_best_song_scores""")
    cursor.execute("""INSERT INTO second_best_song_scores (song_id, difficulty_id, best_score)
                      SELECT b.song_id, b.difficulty_id, MAX(b.best_score) AS best_score
                      FROM best_scores as b, charts_song, best_song_scores as b2
                      WHERE b.song_id=charts_song.name AND charts_song.verified=1 AND
                            b2.song_id=b.song_id AND b2.difficulty_id=b.difficulty_id AND
                            b2.best_score>b.best_score
                      GROUP BY b.song_id, b.difficulty_id
                      ORDER BY b.best_score DESC""")

    # Create a table for the third best song scores by difficulty
    cursor.execute("""DELETE FROM third_best_song_scores""")
    cursor.execute("""INSERT INTO third_best_song_scores (song_id, difficulty_id, best_score)
                      SELECT b.song_id, b.difficulty_id, MAX(b.best_score) AS best_score
                      FROM best_scores as b, charts_song, second_best_song_scores as b2
                      WHERE b.song_id=charts_song.name AND charts_song.verified=1 AND
                            b2.song_id=b.song_id AND b2.difficulty_id=b.difficulty_id AND
                            b2.best_score>b.best_score
                      GROUP BY b.song_id, b.difficulty_id
                      ORDER BY b.best_score DESC""")

    # Create tables for listing gold, silver and bronze metal counts
    cursor.execute("""DELETE FROM player_gold_medals""")
    cursor.execute("""INSERT INTO player_gold_medals (player_id, `count`)
                      SELECT s1.player_id, sum(1) as count FROM best_scores AS s1, best_song_scores AS s2
                      WHERE s1.best_score=s2.best_score AND s1.song_id=s2.song_id AND s1.difficulty_id=s2.difficulty_id
                      GROUP BY s1.player_id""")

    cursor.execute("""DELETE FROM player_silver_medals""")
    cursor.execute("""INSERT INTO player_silver_medals (player_id, `count`)
                      SELECT s1.player_id, sum(1) as count FROM best_scores AS s1, second_best_song_scores AS s2
                      WHERE s1.best_score=s2.best_score AND s1.song_id=s2.song_id AND s1.difficulty_id=s2.difficulty_id
                      GROUP BY s1.player_id""")

    cursor.execute("""DELETE FROM player_bronze_medals""")
    cursor.execute("""INSERT INTO player_bronze_medals (player_id, `count`)
                      SELECT s1.player_id, sum(1) as count FROM best_scores AS s1, third_best_song_scores AS s2
                      WHERE s1.best_score=s2.best_score AND s1.song_id=s2.song_id AND s1.difficulty_id=s2.difficulty_id
                      GROUP BY s1.player_id""")

    cursor.execute("""COMMIT""")
    cursor.fetchall()
  except Exception, e:
    cursor.execute("""ROLLBACK""")
    raise

# Initialize the cache tables
#refreshCache()
