from fretsweb.charts.models import Song, Score

#
# Merge duplicate songs that have the same hash but different name
#
def mergeSongs():
  dupSongCount  = 0
  dupScoreCount = 0
  
  for song in Song.objects.filter(verified = True):
    print "Duplicates for %s:" % song.name
    for dupSong in Song.objects.filter(hash = song.hash, verified = False):
      print "  %s (%d)" % (dupSong.name, dupSong.score_set.count())
      dupSongCount += 1

      for score in dupSong.score_set.all():
        score.song = song
        score.save()
        dupScoreCount += 1

      if dupSong.score_set.count() == 0:
        dupSong.delete()

  print "%d duplicate songs with %d scores found." % (dupSongCount, dupScoreCount)

#
# Top unverified songs
#
def topNewSongs(count = 20):
  songs = Song.objects.filter(verified = False)
  songs = sorted(songs, key = lambda song: -song.score_set.count())

  for song in songs[:count]:
    print "%s (%d plays)" % (song, song.score_set.count())
