import Cerealizer
import binascii
import sha
import datetime
import os
from fretsweb.charts.models import Score, Difficulty
from fretsweb.settings import MEDIA_ROOT

class ScoreException(Exception):
  pass

class HashException(Exception):
  pass

def calculateScoreHash(difficultyId, score, stars, name):
  return sha.sha("%d%d%d%s" % (difficultyId, score, stars, name)).hexdigest()

def safeString(s):
  badChars = {
    "<": "",
    ">": "",
  }
  for c, r in badChars.items():
    s = s.replace(c, r)
  return s

def decodeScores(scores):
  scores        = Cerealizer.loads(binascii.unhexlify(scores))
  decodedScores = []
  for difficultyId in scores.keys():
    for score, stars, name, hash in scores[difficultyId]:
      # Coerce the values to the proper types
      difficultyId = int(difficultyId)
      name         = safeString(name)
      hash         = safeString(hash)
      stars        = int(stars)
      score        = int(score)
      
      # Check the hash
      if calculateScoreHash(difficultyId, score, stars, name) == hash:
        # For an absolutely perfect play, the game sometimes reports six stars due to a rounding error
        if stars == Score.MAX_STARS + 1:
          stars = Score.MAX_STARS
        # Check that contents make sense
        if stars < 0 or stars > Score.MAX_STARS or score < 0:
          raise ScoreException("Suspicious score: %d stars, %d points" % (stars, score))
        try:
          difficulty = Difficulty.objects.get(identifier = difficultyId)
        except Difficulty.DoesNotExist:
          raise ScoreException("Bad difficulty id")
        decodedScores.append((difficulty, score, stars, name))
      else:
        raise HashException("Hash mismatch")
  if not scores:
    raise ScoreException("Empty scores")
  if len(scores) > 5:
    raise ScoreException("Too many scores")
  return decodedScores

def getArtistAndTitleForSong(songId):
  # Parse "Artist - Title" format
  if " - " in songId:
    artist, title = songId.split(" - ", 1)
    return artist.strip(), title.strip()

  # Find a matching song from the midi map
  midiMap = os.path.join(MEDIA_ROOT, "ghmidimap.txt")
  if os.path.isfile(midiMap):
    for line in open(midiMap):
      songName, fullName, artist = map(lambda s: s.strip(), line.strip().split(";"))
      if songId == songName:
        return artist, fullName

  # No luck
  return "", songId
