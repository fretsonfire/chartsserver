from django.core.paginator import ObjectPaginator, InvalidPage

MAX_PER_PAGE = 20

def search(request, objectClass, searchAttribute):
  query = request.GET.get("query", "")

  if query:
    attrs   = {searchAttribute: query}
    objects = objectClass.objects.filter(**attrs)
  else:
    objects = objectClass.objects.all()
  
  namespace = {
    "query": query
  }
  return namespace, objects

def paginate(request, objects):
  paginator  = ObjectPaginator(objects, MAX_PER_PAGE)
  page       = int(request.GET.get("page", 1)) - 1
  objectList = paginator.get_page(page)

  namespace = {
    "hasMultiplePages": paginator.pages > 1,
    "objectList":       objectList,
    "hasNextPage":      paginator.has_next_page(page),
    "hasPreviousPage":  paginator.has_previous_page(page),
    "currentPage":      page + 1,
    "nextPage":         page + 2,
    "previousPage":     page,
    "lastPage":         paginator.pages,
    "pages":            xrange(1, paginator.pages + 1),
  }

  if paginator.pages > 10:
    pages = range(max(page - 5, 1), min(page + 5, paginator.pages + 1))
    if pages[-1] != paginator.pages:
      pages.append(paginator.pages)
    if pages[0]  != 1:
      pages = [1] + pages
    if pages[0] != pages[1] - 1:
      namespace["firstThreshold"] = pages[1]
    if pages[-2] != pages[-1] - 1:
      namespace["secondThreshold"] = pages[-1]
    namespace["pages"] = pages
  return namespace
