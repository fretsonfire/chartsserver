from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^$',                           'fretsweb.charts.views.charts'),
    (r'^songs/$',                     'fretsweb.charts.views.songList'),
    (r'^songs/(?P<songId>.+)/$',      'fretsweb.charts.views.song'),
    (r'^players/$',                   'fretsweb.charts.views.playerList'),
    (r'^players/(?P<playerId>.+)/$',  'fretsweb.charts.views.player'),
    (r'^documentation',               'fretsweb.charts.views.documentation'),
    (r'^top-unverified/',             'fretsweb.charts.views.topUnverifiedSongs'),
    (r'^refresh/',                    'fretsweb.charts.views.refresh'),
    (r'^merge-duplicates/',           'fretsweb.charts.views.mergeDuplicateSongs'),
)
