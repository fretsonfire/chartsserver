import unittest
import datetime
from django.test.client import Client
from fretsweb.charts.song import getArtistAndTitleForSong
from fretsweb.charts.models import Song, Player, Difficulty, Score
from fretsweb import log

log.quiet = True

class SongTest(unittest.TestCase):
  def testSongRecognition(self):
    # Known song
    artist, title = getArtistAndTitleForSong("symphofdestruction")
    self.assertEquals(artist, "Megadeth")
    self.assertEquals(title, "Symphony of Destruction")

    # Nicely formatted name
    artist, title = getArtistAndTitleForSong("Song Artist - Song Title")
    self.assertEquals(artist, "Song Artist")
    self.assertEquals(title, "Song Title")

    # Unrecognizable
    artist, title = getArtistAndTitleForSong("tuuba")
    self.assertEquals(artist, "")
    self.assertEquals(title, "tuuba")

class ModelTest(unittest.TestCase):
  def setUp(self):
    # Create some dummy data
    self.difficulty = Difficulty.objects.create(name = "Normal", identifier = 1)
    self.player     = Player.objects.create(name = "Basisti")
    self.song       = Song.objects.create(name = "song", title = "Lift", artist = "Poets of the Fall", hash = "dadacafe")
    self.score      = Score.objects.create(song = self.song, player = self.player, date = datetime.datetime.now(), \
                                           difficulty = self.difficulty, score = 1234, stars = 3)

  def tearDown(self):
    for cls in [Song, Player, Difficulty, Score]:
      cls.objects.all().delete()

  def testDifficultyDeletion(self):
    assert Score.objects.all().count() == 1
    self.difficulty.delete()
    assert Score.objects.all().count() == 0

  def testPlayerDeletion(self):
    assert Score.objects.all().count() == 1
    self.player.delete()
    assert Score.objects.all().count() == 0

  def testSongDeletion(self):
    assert Score.objects.all().count() == 1
    self.song.delete()
    assert Score.objects.all().count() == 0

class ViewTest(unittest.TestCase):
  def setUp(self):
    # Every test needs a client
    self.client = Client()
    
    # Create some dummy data
    self.difficulty1 = Difficulty.objects.create(name = "1", identifier = 1)
    self.difficulty2 = Difficulty.objects.create(name = "2", identifier = 2)
    self.difficulty3 = Difficulty.objects.create(name = "3", identifier = 3)
    self.difficulty4 = Difficulty.objects.create(name = "4", identifier = 4)
    self.player      = Player.objects.create(name = "Basisti")
    self.song        = Song.objects.create(name = "song", title = "Lift", artist = "Poets of the Fall", hash = "dadacafe")
    self.scoreString = "63657265616c310a340a646963740a6c6973740a7475706c650a340a6939353930380a69350a73340a62616c6c7334300a616530323366626336626634613736346664626130343233656137626238303164383062646535317475706c650a340a6939353930380a69350a73340a62616c6c7334300a61653032336662633662663461373634666462613034323365613762623830316438306264653531310a72310a69320a320a72320a72330a72300a"

  def tearDown(self):
    for cls in [Song, Player, Difficulty, Score]:
      cls.objects.all().delete()
      
  def testScoreSubmission(self):
    # Submit a bunch of scores
    attrs = {
      "scores": self.scoreString,
      "version": "1.2.324",
      "songName": "song",
      "songHash": "dadacafe",
    }
    response = self.client.get('/fretsonfire/play/', attrs)

    # Check that the submission succeeded
    self.failUnlessEqual(response.status_code, 200)
    self.failUnlessEqual(response.content, "True")
    self.failUnlessEqual(Score.objects.all().count(), 1)

    # Submit the same thing again and check that it was rejeced properly
    response = self.client.get('/fretsonfire/play/', attrs)

    # Check that the submission succeeded
    self.failUnlessEqual(response.status_code, 200)
    self.failUnlessEqual(response.content, "True")
    self.failUnlessEqual(Score.objects.all().count(), 1)

  def testHashCollision(self):
    # Submit a bunch of scores
    attrs = {
      "scores": self.scoreString,
      "version": "1.2.324",
      "songName": "song2",
      "songHash": "dadacafe",
    }
    response = self.client.get('/fretsonfire/play/', attrs)

    # Check that the submission succeeded
    self.failUnlessEqual(response.status_code, 200)
    self.failUnlessEqual(response.content, "True")
    self.failUnlessEqual(Score.objects.all().count(), 1)
    self.failUnlessEqual(Song.objects.all().count(), 1)
